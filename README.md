# Changelog

A changelog generator for Gitea

## Purpose

This repo is currently part of Gitea. The purpose of it is to generate a changelog when writing release notes.  
This project was made for Gitea, so while you are free to use it in your own projects, it is subject to change with Gitea.  
This tool generates a changelog from PRs based on their milestone and labels.

## Installation

Download a pre-built binary from our [downloads page](https://dl.gitea.com/changelog-tool/) or clone the source and follow the [building instructions](#building).

## Configuration

See the [changelog.example.yml](config/changelog.example.yml) example file.

## Usage

### Changelog Entries

```sh
changelog generate -m=1.11.0 -c=/path/to/my_config_file
```

### Contributors List

```sh
changelog contributors -m=1.11.0 -c=/path/to/my_config_file
```

## Building

```sh
go build
```

## Contributing

Fork -> Patch -> Push -> Pull Request

## Authors

* [Maintainers](https://gitea.com/org/gitea/members)
* [Contributors](https://gitea.com/gitea/changelog/commits/branch/main)<!-- FIXME when contributors page works -->

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for the full license text.
