// Copyright 2018 The Gitea Authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package cmd

import (
	"os"
	"path/filepath"

	"github.com/urfave/cli/v3"
)

var (
	// Version of changelog
	Version        = "development"
	milestoneFlag  string
	tagFlag        string
	configPathFlag string
	tokenFlag      string
	detailsFlag    bool
	afterFlag      int64
	issuesFlag     bool
)

// New returns a new changelog App
//
//nolint:funlen
func New() *cli.App {
	app := &cli.App{
		Name:    "changelog",
		Usage:   "Changelog tools for Gitea",
		Version: Version,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "milestone",
				Aliases:     []string{"m"},
				Usage:       "Targeted milestone",
				Destination: &milestoneFlag,
				Persistent:  true,
			},
			&cli.StringFlag{
				Name:        "tag",
				Aliases:     []string{"T"},
				Usage:       "Git tag for milestone url, if not set milestone is used",
				Destination: &tagFlag,
				Persistent:  true,
			},
			&cli.StringFlag{
				Name:        "config",
				Aliases:     []string{"c"},
				Usage:       "Specify a config file",
				Value:       getDefaultConfigFile(),
				Destination: &configPathFlag,
				Persistent:  true,
			},
			&cli.StringFlag{
				Name:        "token",
				Aliases:     []string{"t"},
				Usage:       "Access token for private repositories/instances",
				Destination: &tokenFlag,
				Persistent:  true,
			},
			&cli.BoolFlag{
				Name:        "details",
				Aliases:     []string{"d"},
				Usage:       "Generate detail lists instead of long lists",
				Destination: &detailsFlag,
				Persistent:  true,
			},
			&cli.Int64Flag{
				Name:        "after",
				Aliases:     []string{"a"},
				Usage:       "Only select PRs after a given index (continuing a previous changelog)",
				Destination: &afterFlag,
				Persistent:  true,
			},
			&cli.BoolFlag{
				Name:        "issues",
				Aliases:     []string{"i"},
				Usage:       "Generate changelog from issues (otherwise from pulls)",
				Destination: &issuesFlag,
				Persistent:  true,
			},
		},
		Commands: []*cli.Command{
			Generate,
			Contributors,
			Init,
		},
	}
	return app
}

func getDefaultConfigFile() string {
	pwd, err := os.Getwd()
	if err != nil {
		return ""
	}
	config := filepath.Join(pwd, ".changelog.yml")
	info, err := os.Stat(config)
	if err == nil && !info.IsDir() {
		return config
	}
	return ""
}
