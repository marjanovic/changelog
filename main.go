// Copyright 2018 The Gitea Authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"os"

	"code.gitea.io/changelog/cmd"
)

func main() {
	app := cmd.New()
	if err := app.Run(os.Args); err != nil {
		fmt.Printf("Failed to run app with %s: %v\n", os.Args[1:], err)
	}
}
